use std::ffi::{CStr, CString, OsStr, OsString};
use std::fs::{self, Metadata};
use std::io;
use std::os::unix::ffi::{OsStrExt, OsStringExt};
use std::os::unix::prelude::{MetadataExt, PermissionsExt};
use std::path::{Path, PathBuf};

use std::mem;

use std::ptr;

use fuse_mt::*;
use time::Timespec;

pub struct AnnexSizeFS {
    pub target: OsString,
}

fn filetype_to_filetype(ft: fs::FileType) -> FileType {
    if ft.is_dir() {
        FileType::Directory
    } else if ft.is_symlink() {
        FileType::Symlink
    } else {
        FileType::RegularFile
    }
}

const fn time(sec: i64, nanos: i64) -> Timespec {
    Timespec {
        sec,
        nsec: nanos as i32,
    }
}
const TM0: Timespec = time(0, 0);

fn meta_attr(m: Metadata) -> FileAttr {
    FileAttr {
        size: m.len(),
        blocks: m.blocks(),
        atime: time(m.atime(), 0),
        mtime: time(m.mtime(), 0),
        ctime: time(m.ctime(), 0),
        crtime: TM0,
        kind: filetype_to_filetype(m.file_type()),
        perm: (m.permissions().mode() & 0o7777) as u16,
        nlink: 0,
        uid: m.uid(),
        gid: m.gid(),
        rdev: 0,
        flags: 0,
    }
}

fn vfattr(size: u64, uid: u32, gid: u32) -> FileAttr {
    FileAttr {
        size,
        blocks: size / 512,
        atime: TM0,
        mtime: TM0,
        ctime: TM0,
        crtime: TM0,
        kind: FileType::RegularFile,
        perm: 0,
        nlink: 0,
        uid,
        gid,
        rdev: 0,
        flags: 0,
    }
}

fn statfs_to_fuse(statfs: libc::statfs) -> Statfs {
    Statfs {
        blocks: statfs.f_blocks as u64,
        bfree: statfs.f_bfree as u64,
        bavail: statfs.f_bavail as u64,
        files: statfs.f_files as u64,
        ffree: statfs.f_ffree as u64,
        bsize: statfs.f_bsize as u32,
        namelen: statfs.f_namelen as u32,
        frsize: statfs.f_frsize as u32,
    }
}

impl AnnexSizeFS {
    fn real_path(&self, partial: &Path) -> OsString {
        PathBuf::from(&self.target)
            .join(partial.strip_prefix("/").unwrap())
            .into_os_string()
    }

    fn stat_real(&self, path: &Path, uid: u32, gid: u32) -> io::Result<FileAttr> {
        let real: OsString = self.real_path(path);
        debug!("stat_real: {:?}", real);
        let m = fs::symlink_metadata(&real)?;
        debug!("m {:?}", m);
        let mut r = real.into_vec();
        r.push(0);
        let r = CStr::from_bytes_with_nul(&r).unwrap();
        if m.is_symlink() {
            let mut bu = [0u8; libc::PATH_MAX as usize];
            if let Some(size) = readlink(r, &mut bu).and_then(|s| {
                let p = Path::new(s);
                let f = p.file_name()?.to_str()?;
                debug!("f {:?}", f);
                let f = f.strip_prefix("SHA256E-s")?;
                debug!("f {:?}", f);
                let e = f.find(|c: char| !c.is_ascii_digit())?;
                let s = &f[..e];
                s.parse().ok()
            }) {
                return Ok(vfattr(size, uid, gid));
            }
        }
        Ok(meta_attr(m))
    }
}

const TTL: Timespec = Timespec { sec: 1, nsec: 0 };

impl FilesystemMT for AnnexSizeFS {
    fn init(&self, _req: RequestInfo) -> ResultEmpty {
        debug!("init");
        Ok(())
    }

    fn destroy(&self, _req: RequestInfo) {
        info!("destroy");
    }

    fn getattr(&self, req: RequestInfo, path: &Path, _fh: Option<u64>) -> ResultEntry {
        debug!("getattr: {:?}", path);
        match self.stat_real(path, req.uid, req.gid) {
            Ok(attr) => Ok((TTL, attr)),
            Err(e) => Err(e.raw_os_error().unwrap()),
        }
    }

    fn opendir(&self, _req: RequestInfo, path: &Path, _flags: u32) -> ResultOpen {
        let real = self.real_path(path);
        debug!("opendir: {:?} (flags = {:#o})", real, _flags);
        match opendir(real) {
            Ok(fh) => Ok((fh, 0)),
            Err(e) => {
                let ioerr = io::Error::from_raw_os_error(e);
                error!("opendir({:?}): {}", path, ioerr);
                Err(e)
            }
        }
    }

    fn releasedir(&self, _req: RequestInfo, path: &Path, fh: u64, _flags: u32) -> ResultEmpty {
        debug!("releasedir: {:?}", path);
        closedir(fh)
    }

    fn readdir(&self, _req: RequestInfo, path: &Path, fh: u64) -> ResultReaddir {
        debug!("readdir: {:?}", path);
        let mut entries: Vec<DirectoryEntry> = vec![];

        if fh == 0 {
            error!("readdir: missing fh");
            return Err(libc::EINVAL);
        }

        loop {
            match readdir(fh) {
                Ok(Some(entry)) => {
                    let name_c = unsafe { CStr::from_ptr(entry.d_name.as_ptr()) };
                    let name = OsStr::from_bytes(name_c.to_bytes()).to_owned();
                    if path == OsStr::new("/") && name == ".git" {
                        debug!("skip .git");
                        continue;
                    }
                    let filetype = match entry.d_type {
                        libc::DT_DIR => FileType::Directory,
                        _ => FileType::RegularFile,
                    };

                    entries.push(DirectoryEntry {
                        name,
                        kind: filetype,
                    })
                }
                Ok(None) => {
                    break;
                }
                Err(e) => {
                    error!("readdir: {:?}: {}", path, e);
                    return Err(e);
                }
            }
        }

        Ok(entries)
    }

    fn readlink(&self, _req: RequestInfo, path: &Path) -> ResultData {
        debug!("readlink: {:?}", path);

        let real = self.real_path(path);
        match ::std::fs::read_link(real) {
            Ok(target) => Ok(target.into_os_string().into_vec()),
            Err(e) => Err(e.raw_os_error().unwrap()),
        }
    }

    fn statfs(&self, _req: RequestInfo, path: &Path) -> ResultStatfs {
        debug!("statfs: {:?}", path);

        let real = self.real_path(path);
        let mut buf: libc::statfs = unsafe { ::std::mem::zeroed() };
        let result = unsafe {
            let path_c = CString::from_vec_unchecked(real.into_vec());
            libc::statfs(path_c.as_ptr(), &mut buf)
        };

        if -1 == result {
            let e = io::Error::last_os_error();
            error!("statfs({:?}): {}", path, e);
            Err(e.raw_os_error().unwrap())
        } else {
            Ok(statfs_to_fuse(buf))
        }
    }

    fn fsyncdir(&self, _req: RequestInfo, path: &Path, fh: u64, datasync: bool) -> ResultEmpty {
        debug!("fsyncdir: {:?} (datasync = {:?})", path, datasync);

        // TODO: what does datasync mean with regards to a directory handle?
        let result = unsafe { libc::fsync(fh as libc::c_int) };
        if -1 == result {
            let e = io::Error::last_os_error();
            error!("fsyncdir({:?}): {}", path, e);
            Err(e.raw_os_error().unwrap())
        } else {
            Ok(())
        }
    }

    fn unlink(&self, _req: RequestInfo, parent_path: &Path, name: &OsStr) -> ResultEmpty {
        debug!("unlink {:?}/{:?}", parent_path, name);

        let real = PathBuf::from(self.real_path(parent_path)).join(name);
        fs::remove_file(&real).map_err(|ioerr| {
            error!("unlink({:?}): {}", real, ioerr);
            ioerr.raw_os_error().unwrap()
        })
    }

    fn rmdir(&self, _req: RequestInfo, parent_path: &Path, name: &OsStr) -> ResultEmpty {
        debug!("rmdir: {:?}/{:?}", parent_path, name);

        let real = PathBuf::from(self.real_path(parent_path)).join(name);
        fs::remove_dir(&real).map_err(|ioerr| {
            error!("rmdir({:?}): {}", real, ioerr);
            ioerr.raw_os_error().unwrap()
        })
    }
}

macro_rules! into_cstring {
    ($path:expr, $syscall:expr) => {
        match CString::new($path.into_vec()) {
            Ok(s) => s,
            Err(e) => {
                error!(
                    concat!($syscall, ": path {:?} contains interior NUL byte"),
                    OsString::from_vec(e.into_vec())
                );
                return Err(libc::EINVAL);
            }
        }
    };
}

pub fn opendir(path: OsString) -> Result<u64, libc::c_int> {
    let path_c = into_cstring!(path, "opendir");

    let dir: *mut libc::DIR = unsafe { libc::opendir(path_c.as_ptr()) };
    if dir.is_null() {
        return Err(io::Error::last_os_error().raw_os_error().unwrap());
    }

    Ok(dir as u64)
}

pub fn readdir(fh: u64) -> Result<Option<libc::dirent>, libc::c_int> {
    let dir = fh as usize as *mut libc::DIR;
    let mut entry: libc::dirent = unsafe { mem::zeroed() };
    let mut result: *mut libc::dirent = ptr::null_mut();

    let error: i32 = unsafe { libc::readdir_r(dir, &mut entry, &mut result) };
    if error != 0 {
        return Err(error);
    }

    if result.is_null() {
        return Ok(None);
    }

    Ok(Some(entry))
}

pub fn closedir(fh: u64) -> Result<(), libc::c_int> {
    let dir = fh as usize as *mut libc::DIR;
    if -1 == unsafe { libc::closedir(dir) } {
        Err(io::Error::last_os_error().raw_os_error().unwrap())
    } else {
        Ok(())
    }
}

pub fn readlink<'b>(link: &CStr, buf: &'b mut [u8]) -> Option<&'b OsStr> {
    let n = unsafe { libc::readlink(link.as_ptr(), buf.as_mut_ptr() as *mut i8, buf.len()) };
    if n < 0 {
        return None;
    }
    let b = &buf[..n as usize];
    Some(OsStr::from_bytes(b))
}
