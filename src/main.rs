#[macro_use]
extern crate log;
use std::ffi::OsStr;
use std::{env, ffi::OsString};

use annexize::AnnexSizeFS;
use fuse_mt;
use simple_logger::SimpleLogger;

mod annexize;

fn main() {
    SimpleLogger::new()
        .with_level(log::LevelFilter::Info)
        .env()
        .init()
        .unwrap();
    let args: Vec<OsString> = env::args_os().collect();

    if args.len() != 3 {
        println!(
            "usage: {} <git-annex repo> <mountpoint>",
            &env::args().next().unwrap()
        );
        ::std::process::exit(-1);
    }
    let filesystem = AnnexSizeFS {
        target: args[1].clone(),
    };

    let fuse_args = ["-o", "auto_unmount", "-o", "fsname=annexize"]
        .iter()
        .map(|o| o.as_ref())
        .collect::<Vec<&OsStr>>();
    fuse_mt::mount(fuse_mt::FuseMT::new(filesystem, 1), &args[2], &fuse_args).unwrap();
}
